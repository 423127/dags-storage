from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG('mlflow-conda', 
            description='Hello airflow DAG',
            schedule_interval='@once',          
            default_args=default_args
            )

bt = BashOperator(
        task_id = 'print_date2',
        bash_command = 'mlflow run git@bitbucket.org:423127/mlproj.git; sleep 50',
        dag = dag)

bt
