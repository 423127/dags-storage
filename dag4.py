from airflow import DAG
from datetime import datetime, timedelta
from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow import configuration as conf

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2019, 1, 1),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

namespace = 'airflow-cluster'

dag = DAG('test',
          schedule_interval='@once',
          default_args=default_args)


compute_resource = {'request_memory': '2Gi', 'limit_memory': '2Gi'}

with dag:
    k = KubernetesPodOperator(
            namespace='airflow-cluster',
            image="yeonwoo/mlflow-example:test",
            labels={"foo": "bar"},
            cmds= ["/bin/sh"],
            arguments= ["-c", "mlflow run https://github.com/ashukharde3/mlflow-docker-project-example.git; sleep 30"],
            name="mlflow-test-pod",
            task_id="task-one",
            in_cluster=True, # if set to true, will look in the cluster, if false, looks for file
            is_delete_operator_pod=True,
            resources=compute_resource
        )
        
k