from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
from datetime import datetime, timedelta

dag = DAG('mlflow-docker', description='Hello airflow DAG',
        schedule_interval = '* * * * *',
        start_date = datetime(2020,4,7), catchup = False)

bt = BashOperator(
        task_id = 'print_date',
        bash_command = 'mlflow run git@bitbucket.org:423127/mlproj.git; sleep 50',
        dag = dag)

bt
